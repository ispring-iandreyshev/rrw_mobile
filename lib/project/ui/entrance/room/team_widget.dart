import 'package:flutter/material.dart';

import '/generated/l10n.dart';

import '../../../domain/rating/team.dart';
import '../../tools/icons/fraction_icon.dart';

class TeamWidget extends StatelessWidget {
  final Team team;

  const TeamWidget({required this.team});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 44,
      padding: EdgeInsets.only(left: 10, right: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          _buildIcon(),
          _buildName(context),
          _buildOwnerName(context),
        ],
      ),
    );
  }

  Widget _buildIcon() {
    return Flexible(
      flex: 1,
      child: Center(
        child: Icon(
          FractionIcon().teamIcon(team.fractionId),
          color: TeamColor().color(team.id),
        ),
      ),
    );
  }

  Widget _buildName(BuildContext context) {
    return Flexible(
      flex: 3,
      child: Container(
        alignment: Alignment.centerLeft,
        child: Text(
          team.name,
          style: Theme.of(context).primaryTextTheme.subtitle2,
        ),
      ),
    );
  }

  Widget _buildOwnerName(BuildContext context) {
    return Flexible(
      flex: 4,
      child: Container(
        alignment: Alignment.center,
        child: Text(
          '${S.of(context).players} (${team.owner})',
          style: Theme.of(context).primaryTextTheme.headline6,
        ),
      ),
    );
  }
}
