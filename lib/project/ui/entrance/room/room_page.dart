import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

import 'package:mvc_pattern/mvc_pattern.dart';

import '../../../../generated/l10n.dart';

import '../../../presentation/room_controller.dart';

import 'team_widget.dart';
import '../../tools/widgets/swipe_to_refresh_widget.dart';

class RoomPage extends StatefulWidget {
  const RoomPage({Key? key}) : super(key: key);

  @override
  _RoomPageState createState() => _RoomPageState();
}

class _RoomPageState extends StateMVC<RoomPage> {
  RoomController _con = RoomController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            color: Theme.of(context).primaryColor,
            onPressed: () {
              Navigator.pushNamedAndRemoveUntil(
                  context, '/choose-team', (route) => false);
            }),
        title: Text(
          '${S.of(context).room_page_appBar} (${_con.teamStorage.length}/13)',
          style: Theme.of(context).primaryTextTheme.headline1,
        ),
        backgroundColor: Theme.of(context).backgroundColor,
        centerTitle: true,
      ),
      body: RefreshWidget(
        child: LayoutBuilder(
          builder: (context, constraints) => Container(
            height: constraints.maxHeight,
            child: (_con.teamStorage.isNotEmpty)
                ? ListView.separated(
                    physics: const AlwaysScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemBuilder: (BuildContext _, int index) =>
                        TeamWidget(team: _con.teamStorage[index]),
                    separatorBuilder: (BuildContext _, int teamIndex) =>
                        Divider(
                      height: 1.0,
                      indent: Theme.of(context).dividerTheme.indent,
                      endIndent: Theme.of(context).dividerTheme.endIndent,
                      thickness: Theme.of(context).dividerTheme.thickness,
                      color: Theme.of(context).dividerTheme.color,
                    ),
                    itemCount: _con.teamStorage.length,
                    dragStartBehavior: DragStartBehavior.down,
                  )
                : LayoutBuilder(
                    builder: (context, constraints) => Container(
                      height: constraints.maxHeight,
                      child: ListView(
                        padding:
                            EdgeInsets.only(top: constraints.maxHeight / 2),
                        children: [
                          Text(
                            S.of(context).room_is_empty,
                            textAlign: TextAlign.center,
                            style: Theme.of(context).primaryTextTheme.headline1,
                          ),
                        ],
                      ),
                    ),
                  ),
          ),
        ),
        onRefresh: () {
          setState(() {
            _con.setRoomInfo();
          });
          return _con.generateRoomInfo();
        },
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.keyboard_arrow_right_outlined),
        onPressed: () {
          Navigator.pushNamedAndRemoveUntil(context, '/home', (route) => false);
        },
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.miniEndFloat,
    );
  }
}
