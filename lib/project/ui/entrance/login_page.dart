import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:rrw_mvc_refac/project/presentation/login_controller.dart';

import '/generated/l10n.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginPageState();
}

class _LoginPageState extends StateMVC<LoginPage> {
  final LoginController _con = LoginController();
  final _formKey = GlobalKey<FormState>();
  final _loginTextController = TextEditingController();
  final _roomIDController = TextEditingController();
  String? errorText;

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          S.of(context).login_page,
          style: Theme.of(context).primaryTextTheme.headline1,
        ),
        backgroundColor: Theme.of(context).backgroundColor,
        centerTitle: true,
      ),
      body: Container(
        padding: EdgeInsets.all(10.0),
        child: new Form(
          key: _formKey,
          child: new Column(
            children: <Widget>[
              new TextFormField(
                controller: _loginTextController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5)),
                  labelText: S.of(context).text_field_login,
                ),
              ),
              new SizedBox(height: 20.0),
              new TextFormField(
                controller: _roomIDController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5)),
                  labelText: S.of(context).text_field_room_id,
                ),
              ),
              new SizedBox(height: 20.0),
              if (errorText != null) ...[
                Text(
                  errorText!,
                  style: Theme.of(context).primaryTextTheme.headline6,
                ),
                new SizedBox(height: 20.0),
              ],
              new ElevatedButton(
                onPressed: () async {
                  errorText = await _con.auth(
                      _loginTextController.text, _roomIDController.text);
                  if (errorText == null) {
                    Navigator.pushNamedAndRemoveUntil(
                        context, '/choose-team', (route) => false);
                  }
                  setState(() {});
                },
                child: Text(S.of(context).button),
              )
            ],
          ),
        ),
      ),
    );
  }
}
