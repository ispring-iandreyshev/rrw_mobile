import 'package:flutter/material.dart';

import '../../../domain/rating/team.dart';

import '../../tools/icons/fraction_icon.dart';

class TeamWidget extends StatelessWidget {
  final Team team;

  const TeamWidget({required this.team});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 10, right: 10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildIcon(),
          _buildName(context),
          _buildArrow(context),
        ],
      ),
    );
  }

  Widget _buildIcon() {
    return Flexible(
      fit: FlexFit.tight,
      flex: 1,
      child: Center(
        child: Icon(
          FractionIcon().teamIcon(team.fractionId),
          color: TeamColor().color(team.id),
        ),
      ),
    );
  }

  Widget _buildName(BuildContext context) {
    return Flexible(
      fit: FlexFit.tight,
      flex: 10,
      child: Container(
        alignment: Alignment.centerLeft,
        child: Text(
          team.name,
          style: Theme.of(context).primaryTextTheme.subtitle2,
        ),
      ),
    );
  }

  Widget _buildArrow(BuildContext context) {
    return Flexible(
      fit: FlexFit.tight,
      flex: 1,
      child: Container(
        child: Center(
          child: Icon(
            Icons.arrow_forward_ios,
            size: 20,
            color: Theme.of(context).unselectedWidgetColor,
          ),
        ),
      ),
    );
  }
}
