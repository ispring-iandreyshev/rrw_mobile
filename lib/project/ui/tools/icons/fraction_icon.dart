import 'package:flutter/material.dart';

class TeamColor {
  List<Color> teamColor = <Color>[
    Colors.black,
    Colors.blue,
    Colors.green,
    Colors.orange,
    Colors.pink,
    Colors.purple,
    Colors.red,
    Colors.yellow,
    Colors.indigo,
  ];

  TeamColor();

  Color Function(int teamId) get color => _color;

  Color _color(int teamId) {
    return this.teamColor[teamId];
  }
}

class TeamIcons {
  TeamIcons._();

  static const String _fontFam = 'CustomIcons';

  static const IconData road = IconData(0xe804, fontFamily: _fontFam);
  static const IconData pin = IconData(0xe808, fontFamily: _fontFam);
  static const IconData star = IconData(0xe80c, fontFamily: _fontFam);
}

class FractionIcon {
  List<IconData> fractionsTeamIcons = <IconData>[
    TeamIcons.road,
    TeamIcons.star,
    TeamIcons.pin,
  ];

  FractionIcon();

  IconData Function(int fractionId) get teamIcon => _teamIcon;

  IconData _teamIcon(int fractionId) {
    return fractionsTeamIcons[fractionId];
  }
}