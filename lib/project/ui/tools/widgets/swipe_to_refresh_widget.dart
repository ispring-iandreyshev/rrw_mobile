import 'package:flutter/material.dart';

class RefreshWidget extends StatefulWidget {
  final Widget child;
  final Future<void> Function() onRefresh;

  const RefreshWidget({
    required this.child,
    required this.onRefresh,
  });

  @override
  _RefreshWidgetState createState() => _RefreshWidgetState();
}

class _RefreshWidgetState extends State<RefreshWidget> {
  @override
  Widget build(BuildContext context) {
    return buildAndroidRefrashableWidget();
  }

  Widget buildAndroidRefrashableWidget() => RefreshIndicator(
        child: widget.child,
        onRefresh: widget.onRefresh,
      );

  //TODO:
  //можно реализовать SwipeToRefresh под разные платформы
}
