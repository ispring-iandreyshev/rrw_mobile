import 'dart:convert';

//values will be team.idGlobal
//keys - Persons who are playing in team

Map<String, int> roomInfoFromJson(String str) {
  return Map.from(json.decode(str)).map((k, v) {
    return MapEntry<String, int>(k, v);
  });
}

String roomInfoToJson(Map<String, int> data) {
  return json.encode(Map.from(data).map((k, v) {
    return MapEntry<String, dynamic>(k, v);
  }));
}
