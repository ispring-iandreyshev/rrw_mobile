import 'dart:convert';

UserInfo userInfoFromJson(String str) => UserInfo.fromJson(json.decode(str));

class UserInfo{
  final String playerToken;

  UserInfo({required this.playerToken});

  factory UserInfo.fromJson(Map<String, dynamic> json) {
    return UserInfo(
      playerToken: json['playerToken'],
    );
  }  
}