import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:rrw_mvc_refac/generated/l10n.dart';
import 'package:rrw_mvc_refac/project/presentation/post_user_info.dart';
import '../data/server.dart';

class LoginController extends ControllerMVC {
  static late LoginController _this;

  final LoginRepository repo = LoginRepository();

  factory LoginController() {
    _this = LoginController._();
    return _this;
  }

  LoginController._();

  static LoginController get controller => _this;

  bool isValidate(login, roomID) {
    return !(login == '' || roomID == '');
  }

  Future<String?> auth(login, roomID) async {
    String? result;

    try {
      if (isValidate(login, roomID)) {
        result = null;
        final UserInfo userInfo = await repo.userCreate(login, roomID);
        print(userInfo.playerToken);
      } else {
        result = S.current.error1;
      }
    } catch (error) {
      result = S.current.error3;
    }
    return result;
  }
}
