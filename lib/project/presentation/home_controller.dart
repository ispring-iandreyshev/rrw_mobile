import 'package:flutter/material.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import '../domain/home/tab.dart';

class HomeController extends ControllerMVC {
  static late HomeController _this = HomeController._();
  List<TabItem> _tabStack = [];

  final Map<TabItem, GlobalKey<NavigatorState>> _navigatorKeys =
      <TabItem, GlobalKey<NavigatorState>>{
    TabItem.RATING: GlobalKey<NavigatorState>(),
    TabItem.ORDERS: GlobalKey<NavigatorState>(),
    TabItem.EXECUTIONS: GlobalKey<NavigatorState>(),
    TabItem.BANK: GlobalKey<NavigatorState>(),
  };

  TabItem _currentTab = TabItem.RATING;

  factory HomeController() {
    return _this;
  }

  HomeController._();

  static HomeController get controller => _this;
  Map<TabItem, GlobalKey> get navigatorKeys => _navigatorKeys;
  TabItem get currentTab => _currentTab;
  List<TabItem> get tabStack => _tabStack;

  void Function(TabItem tabItem) get selectTab => _selectTab;
  void Function(TabItem tabItem) get pushTab => _pushTab;
  TabItem Function() get popTab => _popTab;

  void _selectTab(TabItem tabItem) {
    this._tabStack.add(tabItem);
    print(this._tabStack.length);
    setState(() {
      _currentTab = tabItem;
    });
  }

  void _pushTab(TabItem tabItem) {
    this._tabStack.add(tabItem);
  }

  TabItem _popTab() {
    this._tabStack.removeLast();
    return this._tabStack.last;
  }
}
