import 'package:mvc_pattern/mvc_pattern.dart';

import '../../generated/l10n.dart';

import '../data/server.dart';
import '../domain/const.dart';

import '../domain/rating/fraction.dart';
import '../domain/rating/team.dart';

class RoomController extends ControllerMVC {
  static late RoomController _this = RoomController._();
  List<Fraction> _fractionStorage = [];
  List<Team> _occupiedTeamStorage = [];
  Map<String, int> _roomInfo = {};

  static RoomController get controller => _this;
  List<Team> get teamStorage => _occupiedTeamStorage;

  Future<void> Function() get generateRoomInfo => _generateRoomInfo;
  void Function() get setRoomInfo => _setRoomInfo;

  factory RoomController() {
    return _this;
  }

  RoomController._() {
    _generateRoomInfo();

    this._fractionStorage = _setDefaultAvailableFraction();
    _initFractions(this._fractionStorage);
  }

  Future<void> _generateRoomInfo() async {
    try {
      _roomInfo =
          await RoomInfoRepository.generateRoomInfo();
    } catch (error) {
      print(error);
    }
  }

  List<Fraction> _setDefaultAvailableFraction() {
    List<Fraction> _fraction = <Fraction>[
      Fraction(
        name: S.current.second_fractionName1,
        id: RR_FRACTION_ID,
        maxTeamAmount: RR_TEAM_AMOUNT,
      ),
      Fraction(
        name: S.current.second_fractionName2,
        id: PR_FRACTION_ID,
        maxTeamAmount: PR_TEAM_AMOUNT,
      ),
      Fraction(
        name: S.current.second_fractionName3,
        id: TK_FRACTION_ID,
        maxTeamAmount: TK_TEAM_AMOUNT,
      ),
    ];

    return _fraction;
  }

  void _initFractions(List<Fraction> fraction) {
    fraction.forEach((Fraction frac) {
      frac.initFraction();
    });    
  }

  void _setRoomInfo() {
    this._occupiedTeamStorage.clear();

    late Team team;

    this._roomInfo.forEach((String owner, int globalTeamId) {
      team = getTeam(globalTeamId);
      team.owner = owner;
      this._occupiedTeamStorage.add(team);
    });
  }

  Team getTeam(int globalTeamId) {
    if (globalTeamId < 6) {
      int realId = globalTeamId;
      return _fractionStorage[RR_FRACTION_ID].teamList[realId];
    } else if (globalTeamId < 9) {
      int realId = globalTeamId - 6;
      return _fractionStorage[PR_FRACTION_ID].teamList[realId];
    } else {
      int realId = globalTeamId - 9;
      return _fractionStorage[TK_FRACTION_ID].teamList[realId];
    }
  }
}