import 'dart:math';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:rrw_mvc_refac/project/presentation/post_user_info.dart';

import '../domain/rating/post_rating.dart';

const String SERVER = 'https://rrw-test.herokuapp.com';

class RateChangeRepository {
  Future<GameRating> getGameRatingInfo() async {
    final Uri url = Uri.parse('$SERVER/game/get-rating');

    final http.Response response = await http.get(url);

    if (response.statusCode == 200) {
      return gameRatingFromJson(response.body);
    } else {
      throw Exception(
          'statusCode: ${response.statusCode}, tried getGameRating');
    }
  }
}

class LoginRepository {
  Future<UserInfo> userCreate(String name, gameToken) async {
    final Uri url = Uri.parse('$SERVER/player/create');

    final http.Response response = await http.post(
      url,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'name': name,
        'gameToken': gameToken,
      }),
    );

    if (response.statusCode == 200) {
      return userInfoFromJson(response.body);
    } else {
      throw Exception('statusCode: ${response.statusCode}, tried userCreate');
    }
  }
}

class AvailableTeamRepository {
  static Future<List<int>> Function() get generateAvailableTeam =>
      _generateAvailableTeam;

  static Future<List<int>> _generateAvailableTeam() async {
    //TODO:
    //ипользовать заместо генерации обращение к серверу

    final Uri url = Uri.parse('$SERVER/game/get-team');

    List<int> availableTeamId = List<int>.generate(13, (index) => index++);

    List<int> generate() {
      int teamAmount = Random().nextInt(13);

      if (teamAmount > 13) {
        return availableTeamId;
      } else if (teamAmount >= 0) {
        teamAmount = 13 - teamAmount;
      }

      for (var i = 0; i < teamAmount; i++) {
        availableTeamId.shuffle();
        availableTeamId.removeLast();
      }

      availableTeamId.sort();

      return availableTeamId;
    }

    return generate();
  }
}

class RoomInfoRepository {
  static Future<Map<String, int>> Function() get generateRoomInfo =>
      _generateRoomInfo;

  static Future<Map<String, int>> _generateRoomInfo() async {
    //TODO:
    //ипользовать заместо генерации обращение к серверу

    final Map<String, int> result = {};

    List<int> occupiedTeamId = _generateOccupiedTeamId();

    occupiedTeamId.forEach((int id) {
      String occupiedTeamNameOwner = _generateTeamNameOwner();

      result[occupiedTeamNameOwner] = id;
    });

    return result;
  }

  static List<int> _generateOccupiedTeamId() {
    List<int> availableTeamId = List<int>.generate(13, (int index) => index++);

    int teamAmount = Random().nextInt(13);

    if (teamAmount > 13) {
      return availableTeamId;
    } else if (teamAmount >= 0) {
      teamAmount = 13 - teamAmount;
    }

    for (var i = 0; i < teamAmount; i++) {
      availableTeamId.shuffle();
      availableTeamId.removeLast();
    }

    return availableTeamId;
  }

  static String _generateTeamNameOwner() {
    String result = '';

    final List<String> nameTemplate = [
      'Алексей',
      'Иван',
      'Рафаиль',
      'Василий',
      'Ярослав',
      'Леонид',
      'Евгений',
      'Мария'
    ];

    final int nameAmount = Random().nextInt(2) + 1;

    for (var i = 0; i < nameAmount; i++) {
      result += '${nameTemplate[Random().nextInt(8)]}';
      if (i != (nameAmount - 1)) {
        result += ', ';
      }
    }

    return result;
  }
}
